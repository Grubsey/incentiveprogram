## Interface: 80000
## Title: Incentive Program
## Notes: Notifies when there are LFG Incentives waiting for you.
## Author: Xubera
## Version: r17
## SavedVariables: IncentiveProgramDB

libs/LibStub.lua
libs/CallbackHandler-1.0.lua
libs/LibDataBroker-1.1.lua
libs/DropDownMenu/UIDropDownMenuTemplates.xml
libs/DropDownMenu/EasyMenu.lua
libs/DropDownMenu/UIDropDownMenu.lua

constants.lua
menu.lua
settings.lua
frame.lua
toast.lua
dungeon.lua
databroker.lua
interfacePanel.lua
core.lua